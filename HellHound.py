#!/usr/bin/python
from requests import ConnectionError
from IllegalArgumentError import IllegalArgumentError
from builders.RequestsBuilder import RequestsBuilder
from consumers.GetContentFromUrlCommand import GetContentFromUrlCommand
from parsers.SrealityParser import SrealityParser
from consumers.GmailSenderCommand import GmailSenderCommand
import logging
from pathlib import Path
import argparse
import Logger

OUTPUT_FILE = 'output.txt'

logger = logging.getLogger('hell-hound-logger')


def parse_args():
    parser = argparse.ArgumentParser(description='Hell hound tool')

    parser.add_argument('--disposition',
                        dest='disposition',
                        action="store",
                        help='Room count',
                        required=True)
    parser.add_argument('--bottom-price',
                        dest='bottomPrice',
                        action="store",
                        help='The lowest price',
                        required=True)
    parser.add_argument('--top-price',
                        dest='topPrice',
                        action="store",
                        help='The top price',
                        required=True)
    parser.add_argument('--offer-type',
                        dest='offerType',
                        action="store",
                        help='sale or lease',
                        required=True)
    parser.add_argument('--estate-type',
                        dest='estateType',
                        action="store",
                        help='apartments or houses',
                        required=True)
    parser.add_argument('--locality',
                        dest='locality',
                        action="store",
                        help='Restriction place',
                        required=True)
    parser.add_argument('--sender-gmail-account',
                        dest='sender',
                        action="store",
                        help='Sender gmail account (ex. sender@gmail.com)',
                        required=True)
    parser.add_argument('--receiver-email-account',
                        dest='to',
                        action="store",
                        help='Receiver email account (ex. receiver@yahoo.com)',
                        required=True)
    args, unknown = parser.parse_known_args()
    return args


def run_task():
    try:
        configuration = parse_args()
        requestsBuilder = RequestsBuilder(configuration.estateType,
                                          configuration.disposition,
                                          configuration.locality,
                                          configuration.locality,
                                          configuration.offerType,
                                          configuration.bottomPrice,
                                          configuration.topPrice)
        srealityQuery = requestsBuilder.get_sreality_query()
        command = GetContentFromUrlCommand(srealityQuery)
        command.execute()
        srealityParser = SrealityParser()
        srealityList = srealityParser.parse(command.responseContent)
        firstLink = get_first_link_from_sreality_list(srealityList)
        check_update(firstLink, configuration.sender, configuration.to)
    except ConnectionError:
        logger.error('Connection error during communication with server.',
                     exc_info=True)
    except UnicodeDecodeError:
        logger.error('Problem with decoding response:', exc_info=True)
    except SystemExit as exc:
        logger.error('Exit code %s', exc.code)
    except Exception:
        logger.error('Application error.', exc_info=True)
    pass


def get_first_link_from_sreality_list(srealityList):
    if len(srealityList) < 1:
        raise IllegalArgumentError("Responses has no item!")
    return srealityList[0].link


def check_link(link):
    isNewLink = False
    fileDir = Path(__file__).resolve().parents[0]
    ouputFilePath = fileDir.joinpath(OUTPUT_FILE)
    with open(ouputFilePath, mode='r+', encoding='utf-8') as output:
        for line in output:
            if line.strip() != link.strip():
                isNewLink = True
                logger.info("New flat notification...")
            else:
                logger.info("Just nothing...")
            break
    return isNewLink


def check_update(link, fromaddr, toaddr):
    fileDir = Path(__file__).resolve().parents[0]
    ouputFilePath = fileDir.joinpath(OUTPUT_FILE)
    logging.info(ouputFilePath)
    if (
        not ouputFilePath.is_file() or not ouputFilePath.stat().st_size > 0
        or check_link(link)
       ):
        with open(ouputFilePath, mode='w+', encoding='utf-8') as output:
            output.write(link)
            output.write('\n')
        msgPlain = str(link)
        subject = 'New flat notification'
        sender = GmailSenderCommand()
        sender.send_message(fromaddr, toaddr, subject, msgPlain)
    pass


if __name__ == "__main__":
    run_task()
    pass
