Hell hound project
=====================

Overview
---------------------

Hell hound project is created to check the newest offer from reality providers such as
[sreality.cz](https://www.sreality.cz).

The nofitication is send by email using gmail api client If new offer is found.

Configuration
---------------------

Configuration parameters are passed as command line paramers. For basic information run following
command.

```console
HellHound.py -h|--help
```

Parameters overview:
<ul>
    <li>DISPOSITION, filter for count of rooms combination</li>
    <li>BOTTOM_PRICE, the lowest price which should be considered</li>
    <li>TOP_PRICE, the top price which should be considered</li>
    <li>OFFER_TYPE, this could be sale or lease</li>
    <li>LOCALITY, restriction to city</li>
    <li>ESTATE_TYPE, apartments, houses</li>
    <li>SENDER_GMAIL_ACCOUNT, sender gmail account</li>
    <li>RECEIVER_EMAIL_ACCOUNT, receiver email account</li>
</ul>

Build
----------------------

As notification service Google gmail api was chosen. It simply sends email with notification when new 
offer is found.

Create gmail account and go to [Google api console](https://console.developers.google.com) for notification service activation. Create your personal project and inside your project create credentials for OAuth 2.0 client. After that export your credentials as client_secret.json to project root directory.

Run project for first time. Then you should be redirected to google webpage where you have to login to your gmail account and allow sending email privileges for your application. Everything should be prepared now.

Run
----------------------

Example of running command.

```python
python HellHound.py --disposition="3+kk|3+1" --bottom-price=0 --top-price=14000 --offer-type=lease --estate-type="apartments" --sender-gmail-account="michael.hrabe@gmail.com" --receiver-email-account="michael.hrabe@gmail.com" --locality="plzen"
```

In case of using built-in scheduler, example is extented with extra argument frequency that simply
represents repeating in seconds

```python
 python Repeat.py --disposition="3+kk|3+1" --bottom-price=0 --top-price=14000 --offer-type=lease --estate-type="apartments" --sender-gmail-account="michael.hrabe@gmail.com" --receiver-email-account="michael.hrabe@gmail.com" --locality="plzen" --frequency=10
```

Docker (In progress)
--------------------

In folder where Dockerfile is located run following command to make image.

```console
docker build -t hell-hound-image .
```
