FROM python:3.7.2-slim

# Installation Directory
ENV INSTALL_PATH /app

# Python will not buffer output to stdout
ENV PYTHONUNBUFFERED 1

# Set the working directory to /app
WORKDIR $INSTALL_PATH

# Copy the current directory contents into the container at /app
COPY . $INSTALL_PATH

# Install requirements for app
RUN pip install --trusted-host pypi.python.org -r requirements.txt

# Expose 587 443
EXPOSE 587 443

# Run repeat.py when the container launches with logs
CMD ["/bin/sh", "run.sh"]

