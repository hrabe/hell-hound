from models.Sreality import Sreality
from bs4 import BeautifulSoup


class SrealityParser:

    def parse(self, htmlTree):
        responses = []
        soup = BeautifulSoup(htmlTree, features='xml')

        for item in soup.rss.channel.findAll('item'):
            link = item.link.text
            description = item.description.text
            title = item.title.text
            sreality = Sreality(link, description, title)
            responses.append(sreality)
        return responses
