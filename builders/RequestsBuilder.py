from builders.SrealityBuilder import SrealityBuilder


class RequestsBuilder:
    def __init__(self, estateType, disposition, localityDistrict,
                 localityRegion, offerType, bottomPrice, topPrice):
        self.estateType = estateType
        self.disposition = disposition
        self.localityDistrict = localityDistrict
        self.localityRegion = localityRegion
        self.offerType = offerType
        self.bottomPrice = bottomPrice
        self.topPrice = topPrice

    def get_sreality_query(self):
        srealityBuilder = SrealityBuilder()
        srealityBuilder.set_estate_type(self.estateType)
        srealityBuilder.set_disposition(self.disposition)
        srealityBuilder.set_offer_type(self.offerType)
        srealityBuilder.set_locality_region(self.localityRegion)
        srealityBuilder.set_locality_district(self.localityDistrict)
        srealityBuilder.set_price_interval(self.bottomPrice, self.topPrice)
        return srealityBuilder.get_full_query()
