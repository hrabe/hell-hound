""" This is sreality builder, it maps https://www.sreality.cz api
    and creates full api request"""


class SrealityBuilder:

    baseApiUrl = "https://www.sreality.cz/api/cs/v2/estates/rss"

    def __init__(self, estateType='', disposition='', offerType='',
                 localityRegion='', localityDistrict='', bottomPrice='',
                 topPrice='', pageCount='', estateAge=''):
        self.set_estate_type(estateType)
        self.set_disposition(disposition)
        self.set_pages(pageCount)
        self.set_offer_type(offerType)
        self.set_locality_region(localityRegion)
        self.set_locality_district(localityDistrict)
        self.set_price_interval(bottomPrice, topPrice)
        self.set_estate_age(estateAge)

    def set_estate_type(self, estateType):
        choices = {'houses': '2', 'apartments': '1'}
        self.estateType = 'category_main_cb=' + choices.get(estateType, '1')
        pass

    def set_price_interval(self, bottomPrice, topPrice):
        self.priceInterval = ('czk_price_summary_order2=' + bottomPrice
                              + '|' + topPrice)
        pass

    def set_disposition(self, disposition):
        choices = {'all-disposition': '2|3|4|5|6',
                   '1+kk': '2',
                   '1+1': '3',
                   '2+kk': '4',
                   '2+1': '5',
                   '3+kk': '6',
                   '3+1': '7',
                   '4+kk': '8'
                   }
        self.disposition = 'category_sub_cb=' + choices.get(disposition, '6')
        pass

    def set_offer_type(self, offerType):
        choices = {'sale': '1', 'lease': '2'}
        self.offerType = 'category_type_cb=' + choices.get(offerType, '2')
        pass

    def set_locality_region(self, localityRegion):
        choices = {'plzensky': '2', 'prazsky': '10'}
        self.localityRegion = ('locality_region_id='
                               + choices.get(localityRegion, '2')
                               )
        pass

    def set_locality_district(self, localityDistrict):
        choices = {'plzen-mesto': '12'}
        self.localityDistrict = ('locality_district_id='
                                 + choices.get(localityDistrict, '12')
                                 )
        pass

    def set_pages(self, perPage):
        choices = {'20': '20',
                   '40': '40',
                   '60': '60'}
        self.perPage = 'per_page=' + choices.get(perPage, '60')

    def set_estate_age(self, estateAge):
        choices = {'past_day': '2', 'past_week': '8'}
        self.estateAge = 'estate_age=' + choices.get(estateAge, '8')

    def get_full_query(self):
        return ('{baseUrl}?{estateType}&{disposition}&{offerType}'
                '&{localityRegion}&{localityDistrict}&{perPage}'
                '&{estateAge}&{priceInterval}'
                ).format(
                         baseUrl=self.baseApiUrl,
                         estateType=self.estateType,
                         disposition=self.disposition,
                         offerType=self.offerType,
                         localityRegion=self.localityRegion,
                         localityDistrict=self.localityDistrict,
                         perPage=self.perPage,
                         estateAge=self.estateAge,
                         priceInterval=self.priceInterval)
