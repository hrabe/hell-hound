#!/usr/bin/python
import time
import HellHound
import schedule
import argparse

parser = argparse.ArgumentParser(description='Hell hound scheduler')
parser.add_argument('--frequency',
                    dest='frequency',
                    action="store",
                    help='Frequency in second',
                    required=True, type=int)
args, unknown = parser.parse_known_args()

schedule.every(args.frequency).seconds.do(HellHound.run_task)

while True:
    try:
        schedule.run_pending()
        time.sleep(1)
    except Exception:
        schedule.clear()
