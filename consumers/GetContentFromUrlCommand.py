import requests


class GetContentFromUrlCommand:

    def __init__(self, url):
        self.url = url

    def execute(self):
        response = requests.get(self.url)
        self.responseContent = response.content
