from __future__ import print_function
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import base64
import oauth2client
from oauth2client import client, tools
import httplib2
from apiclient import errors, discovery
import logging
from pathlib import Path
from oauth2client import file

SCOPES = 'https://www.googleapis.com/auth/gmail.send'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail API Python Send Email'

logger = logging.getLogger('hell-hound-logger')

logger.info('This should go to both console and file')


class GmailSenderCommand:

    def get_credentials(self):

        upTwoFileDir = Path(__file__).resolve().parents[1]
        credentialsDir = upTwoFileDir.joinpath('.credentials')      
        Path(credentialsDir).mkdir(parents=True, exist_ok=True)

        credentialPath = credentialsDir.joinpath('gmail-python-email-send' +
                                                 '.json')
        store = oauth2client.file.Storage(credentialPath)
        credentials = store.get()
        if not credentials or credentials.invalid:
            flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
            flow.user_agent = APPLICATION_NAME
            # argparse fix
            flags = oauth2client.tools.argparser.parse_args(args=[])
            credentials = tools.run_flow(flow, store, flags)
            logger.debug('Storing credentials to ' + credentialPath.absolute())
        return credentials

    def send_message(self, sender, to, subject, msgPlain):
        credentials = self.get_credentials()
        http = credentials.authorize(httplib2.Http())
        service = discovery.build('gmail', 'v1', http=http)
        message1 = self.create_message(sender, to, subject, msgPlain)
        self.send_message_internal(service, "me", message1)

    def send_message_internal(self, service, user_id, message):
        try:
            message = (service.users()
                       .messages()
                       .send(userId=user_id,
                             body=message
                             ).execute())
            logger.info('Message Id: %s' % message['id'])
            return message
        except errors.HttpError as error:
            logger.error('An error occurred: %s' % error, exc_info=True)

    def create_message(self, sender, to, subject, msgPlain):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = sender
        msg['To'] = to
        msg.attach(MIMEText(msgPlain, 'plain'))
        raw = base64.urlsafe_b64encode(msg.as_bytes())
        raw = raw.decode()
        body = {'raw': raw}
        return body
